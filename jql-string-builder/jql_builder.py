import os
import sys
import re
import subprocess

# get input from the user ... where's the text file?
# future state: do this w/ Tkinter in a GUI
# use variable to capture project name rather than hardcode RM but default = RM
print ""
filePath = raw_input("hello, please input the full path to your text file: ")
f = open(filePath, 'r')
blockOfText = f.read()
print("")
projectName = raw_input("what type of tickets are you looking for?: ")
projectName = str(projectName)

# function where the work gets done
def build_jql(input_text):
        # build regex expression as a string so user can specify JIRA project
        # original hardcoded regex used for rm project only
        # r'rm-\d\d\d\d'

        # going to need additional regex search strings here to handle 5 digit rm ticket numbers etc
        search_regex_5 = re.escape(projectName) + r"-\d\d\d\d\d"
        search_regex_4 = re.escape(projectName) + r"-\d\d\d\d"

        # must find a way to build a bigger ticket list variable ... the master list should just be called "ticketList"
        ticketList_5 = re.findall(search_regex_5, input_text, 
                                re.I|re.M)

        # print "input text is: " + input_text
        print "5 digit tickets are: " + str(ticketList_5)

        # iterate through ticketList_5 list and remove those items from the input_text string - trim it down essentially
        # solution is to go through the 5 digit tickets first and then search for remaining 4 digit ticket numbers
        for i in range(len(ticketList_5)):
                forIteration = str(ticketList_5[i][3:])
                print "removing this item from input_text: " + forIteration
                input_text = input_text.replace(forIteration,"BLANK")
        
        # should notice 'removals' or replacements have been made from input_text
        # print "input text is: " + input_text

        # for the input_text here ... it should somehow be less what was found in the preceding regex search
        ticketList_4 = re.findall(search_regex_4, input_text, 
                                re.I|re.M)

        print "4 digit tickets are: " + str(ticketList_4)

        # can't just smash them together like this ... need to do something smarter here
        # if it's a 5 digit ticket number, it should be counted only one time
        ticketList = ticketList_4 + ticketList_5

        # print "master ticketList is: " + str(ticketList)

        #ticketList = re.findall(search_regex_4, input_text, 
        #                        re.I|re.M)

        ticketCountInt = len(ticketList)

        ticketCount = str(len(ticketList))

        if ticketCountInt != 0:
                # change every item in list to upper case; helps identify duplicates
                for i in range(len(ticketList)):
                        ticketList[i] = ticketList[i].upper()

                print ""
                print "i found " + ticketCount + " " + projectName + " tickets in this text file"
                print ""

                # create a set for unique values but convert to list to keep indexing available
                ticketSet = list(set(ticketList))
                uniqueTicketCount = str(len(ticketSet))

                print "of the " + ticketCount + " " + projectName + " tickets, " + uniqueTicketCount + " are unique"
                print ""
                print "building a jql query for you ..."
                print ""

                rm_list = []

                # build the query string using the unique values from the "set list"
                for i in range(len(ticketSet)):
                        ticketSet[i] = ticketSet[i] + ", "
                        rm_list.append(str(ticketSet[i]))
                bigStr = ''.join(rm_list)

                bigStr = (bigStr.strip( ', ' ))

                jqlQuery = "project = " + projectName + " AND issueKey in (" + bigStr + ")"

                write_to_clipboard(jqlQuery)

                print(jqlQuery)
                print ""
                print "happy JIRA\'ing, the query has been copied to your clipboard"
                print ""
                
                # initialize userChoice as empty so we step into the while loop
                userChoice = ""
                
                # use a while loop to ask the user if they want to convert query to linkedIssues
                while userChoice.strip() != 'y' or 'n':
                        userChoice = raw_input("would you like to convert to linkedIssues query (y/n): ")
                        userChoice = userChoice.upper()
                        if userChoice.strip() == 'Y':
                                # call the linkedIssues function, pass in set of tickets
                                for i in range(len(ticketList)):
                                        ticketList[i] = ticketList[i].strip(',')
                                ticketSet = list(set(ticketList))
                                build_linked_issues(ticketSet)
                                break
                        if userChoice.strip() == 'N':
                                print("")
                                print("ok, no hard feelings")
                                print("")
                                break

        else:
                print ""
                print "I didn't find any " + projectName + " tickets in that file"
                print ""


def write_to_clipboard(output):
        process = subprocess.Popen(
                'pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
        process.communicate(output.encode('utf-8'))

def build_linked_issues(set_of_tickets):
        rm_list = []
        for i in range(len(set_of_tickets)):
                set_of_tickets[i] = "linkedIssues(" + set_of_tickets[i] + ")" + ", "
                rm_list.append(str(set_of_tickets[i]))
        bigStr = ''.join(rm_list)
        bigStr = (bigStr.strip( ', ' ))
        jqlQuery = "project = " + projectName + " AND issueKey in (" + bigStr + ")" + " AND issuetype = \"GRC_Control Activity\""
        write_to_clipboard(jqlQuery)
        print ""
        print(jqlQuery)
        print ""
        print "the query has been converted and copied to your clipboard"
        print ""

# don't forget to call the function
build_jql(blockOfText)