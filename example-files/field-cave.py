from sys import argv 

user_name = argv[1]

def start():
	print "You stand alone in a gorgeous meadow"
	print "Your name is: %s" % user_name
	print "You notice a cave looming before you"
	print "Will you enter the cave?"
	print "1. of course, ain't skeered"
	print "2. no, because i suck"
	
	choice = raw_input("> ")
	
	if "1" in choice:
		cave("yes")
	elif "2" in choice:
		gameover("ok, we'll just hang out here and be boring then")
	else:
		gameover("dude, just enter a 1 or 2")
		
def cave(firstAnswer):
	print "You answered", firstAnswer
	print "Good first step."
	print "There are 3 monsters in here"
	print "... a Griffon"
	print "... Cerberus"
	print "... and, a Unicorn named Bob"
	print "which one will you immediately slay?"
	
	choice = raw_input("> ")
	
	if "griffon" in choice:
		griffonFight("glorious!")
	elif "cerberus" in choice:
		cerberusFight()
	elif "unicorn" in choice:
		unicornFight()
	else:
		cave("... poorly the first time, try again")

def griffonFight(catchPhrase):
	print "You chose to slay the mighty griffon,", catchPhrase
	
def cerberusFight():
	print "You're going to take on Cerberus huh?"

def unicornFight():
	print "You chose to kill Bob? You monster!"
		
def gameover(why):
	print why, "... lame!"
	exit(0)

start()