def while_loop(arg1, arg2):
	i = 0
	numbers = []

	while i < arg1:
		print "At the top i is %d" % i
		numbers.append(i)

		i += arg2
		print "Numbers now: ", numbers
		print "At the bottom i is %d" % i


	print "The numbers: "

	for num in numbers:
		print num

loop = int(raw_input("> ok, how many times should we loop? "))
increment = int(raw_input("> and, what should we increment by? "))

while_loop(loop, increment)