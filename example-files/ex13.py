from sys import argv
done = False

script, first, second, third = argv

print "The script is called:", script
print "Your first variable is:", int(first)
print "Your second variable is:", second
print "Your third variable is:", third

done = bool(raw_input("Press 'Enter' to continue."))

# figure out how to use a while loop
while not done:
	# practice using raw_input and prompting user
	# also practice with conversion to int for math reasons
	x = int(raw_input("what is x? "))

	simple_maths = int(first) * x

	print "first variable times x is", simple_maths
	break