print "I will now count my chickens:"

# important to remember the order of operations! PEMDAS.
# this line does division first, then addition
print "Hens", 25 + 30 / 6
# modulus (%) is a type of division ... just think, remainder
print "Roosters", 100 - 25 * 3 % 4

print "Now I will count the eggs:"

# using parantheses here would make this easier to read!
# the most confusing bit is that 1/4=0 due to variable type int
print 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6

print "Is it true that 3 + 2 < 5 - 7?"

print 3 + 2 < 5 - 7

print "What is 3 + 2?", 3 + 2
print "What is 5 - 7?", 5 - 7 

print "Oh, that's why it's False."

print "How about some more?"

print "Is it greater?", 5 > -2 
print "Is it greater or equal?", 5 >= -2 
print "Is it less or equal?", 5 <= -2 