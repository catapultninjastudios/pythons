# Random Number Generator
# Leaving another comment here ;)
from random import choice

# Helper function to input a number within a given range
def input_number(prompt, min_value, max_value):
	value = None
	while value is None:
		try:
			value = int(raw_input(prompt))
		except ValueError:
			print 'please enter a number!'
		if value < min_value or value > max_value:
			print ('please enter a number between %i and %i!' % (min_value, max_value))
			value = None
	return value

# Print the title and input the range of numbers
title = 'Random Number Generator'
print title
print '=' * 40 # for formatting purposes
minimum = input_number('Smallest Number: ', 1, 9999)
maximum = input_number('Largest Number: ', minimum, 9999)
n = input_number('How many numbers do you want to draw? ', 1, maximum - minimum + 1)

#Pick the numbers and print the results
all_numbers = range(minimum, maximum + 1)
selection = []
for i in xrange(n):
	r = choice(all_numbers)
	selection.append(r)
	all_numbers.remove(r)
print '=' * 40
print 'Your', len(selection), 'numbers are:', selection
