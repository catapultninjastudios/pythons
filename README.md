# README #

There are various Python files here. Everything is written in Python 2.7.x ... the most work has been done on the jql-string-builder

jql-string-builder reads a text file and creates a jql query for use on EAC/J - the query is specific to the risk management project and is useful if you need to write a JIRA query that returns information on a bunch of RM tickets. Hopefully this tool saves you sheer minutes of time and frustration.

### What Does "jql-string-builder" Do? ###
* read text file (path provided by user)
* scrape through and pull out all strings that begin with "RM-" in any variation of case (i.e. rM-, rm-, Rm-, RM-)
* organize all items in a list; de-duplicate
* build jql query string and copy it to the clipboard

### How do I get set up? ###

* Make sure you've got Python 2.7.x installed ... should be the case by default if you're running Mac OS X, type: python -V in a terminal window to see your version
* Need to download Python? go here: [https://www.python.org/downloads/](https://www.python.org/downloads/)
* Clone this repository locally
* Navigate to your local jql-string-builder directory in a terminal window
* type: python jql_builder.py
* enter the path/name of your text file with lots of RM ticket numbers somewhere in it
* paste the jql query on EAC/J